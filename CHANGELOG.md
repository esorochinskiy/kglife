## 1.0-12
### Eugene E. Sorochinskiy <manager@darkguard.net>  Mon, 16 Dec 2024

* analysys added

## 1.0-11
### Eugene E. Sorochinskiy <manager@darkguard.net>  Sat, 09 Oct 2024

* no X11 Extras


## 1.0-10
### Eugene E. Sorochinskiy <manager@darkguard.net>  Sat, 09 Oct 2024

* when trying to launch second copy just raise main window if app is already running

## 1.0-9
### Eugene E. Sorochinskiy <manager@darkguard.net>  Sat, 02 Oct 2024

* context menu hides unnecessary items
* save selection

## 1.0-8
### Eugene E. Sorochinskiy <manager@darkguard.net> Wed, 03 Jan 2024

* copy-paste implemented
* selection context menu
* minor bug fix

## 1.0-7
### Eugene E. Sorochinskiy <manager@darkguard.net>  Fri, 15 Dec 2023

* Random pattern generator

## 1.0-6
### Eugene E. Sorochinskiy <manager@darkguard.net> Sun, 10 Dec 2023 09:39:49

* Supports work with different file formats: native, XLife RLE, CELLS
* Pattern preview in config dialog
* Ititial timer slider setup
* Printer support
* Author tipo fix
* KDE look-and-feel
* Save As option added
* Bug fixes

## 1.0-5
### Eugene E. Sorochinskiy <manager@darkguard.net>  Mon, 13 Nov 2023

* Bug fix: choosing default colors sets incorrect background
* Smart pointer prevent memory leak
* Advanced settings dialog templates added help improved
* CMake error fix


## 1.0-4
### Eugene E. Sorochinskiy <manager@darkguard.net> Thu, 19 Oct 2023

* Interface icons fix look brighter on light themes

## 1.0-3
### Eugene E. Sorochinskiy <manager@darkguard.net> Wed, 18 Oct 2023

* Interface fix: field move, controls hide/show

## 1.0-2
### Eugene E. Sorochinskiy <manager@darkguard.net> Sat, 14 Oct 2023

* Interface fix: field limits

## 1.0-1
### Eugene E. Sorochinskiy <manager@darkguard.net> Tue, 03 Oct 2023

* Initial release
